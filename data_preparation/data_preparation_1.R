#script 1 for data preparation 
#includes: 
#1. Merging of datatables for plasma cholinesterase, other labmarkers and antropometrics 
#2. Merging with mrt_pseudonym and SIC for future identification
#3. Definition of hypertension and diabetes group (for control variables) 
#4. Definition of anticholinergic medication and che inhibitors (for exclusion)
#5. Definition of disrupted liver function (for exclusion)
#6. Apply exclusion 
#7. Data preparation sample 1
#8. Data preparation sample 2

library(dplyr)
library(tidyverse) #includes ggplot2, dplyr, tidyr, readr, tibble, stringr ...
library(data.table)
library(naniar) # helps to deal with NA
library(stringr)
library(readxl)

################################################################################
#1. Merging
################################################################################

#read anthropometric dataset, original file, n=2667 ----------------------------
life <- read_xlsx(paste0("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/data_2020_11_02/PV0489_datajoin_2020-02-11.xlsx"))
colnames(life)[1] <- "SIC" # renames column 1

#read cholinesterase dataset, n=1532  ------------------------------------------
che <- read.csv(paste0("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/cholinesterases/cholinesterase_activity_LIFE.csv")) 

#two pv_pseudonyms (called here "SIC") make problems due to readability 
#problem cannot be solved in the original df which is the che df (does not save the text formatted cells)
#this is just the biggest pain!!
# 9.74600000000000E+299 = 97460E0295
# 4.06133500000000E+65 = 4061335E59

#rename them
che$SIC[che$SIC == '9.746E+299'] <- '97460E0295'
che$SIC[che$SIC == '4.061335E+065'] <- '4061335E59'

#3 more pv_pseudonyms make problems
# 563749144 = 0563749144
# 973772939 = 0973772939
# 745421040 = 0745421040

che$SIC[che$SIC == '563749144'] <- '0563749144'
che$SIC[che$SIC == '973772939'] <- '0973772939'
che$SIC[che$SIC == '745421040'] <- '0745421040'


#read additional laboratory markers dataset, n=2600 ----------------------------
labm <- '/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/laboratory_markers_2023_06_05/'

### HbA1C --> T00440, n=2629
hba1c <- read_xlsx(paste0(labm,'PV0489_T00440_NODUP','.xlsx'))
###ALAT --> T00445, n=2639
alat <- read_xlsx(paste0(labm,'PV0489_T00445_NODUP','.xlsx'))
###ASAT --> T00446, n=2639
asat <- read_xlsx(paste0(labm,'PV0489_T00446_NODUP','.xlsx'))
###GGT --> T00448, n=2620
ggt <- read_xlsx(paste0(labm,'PV0489_T00448_NODUP','.xlsx'))
###Bilirubin total, n=2649
bili <- read_xlsx(paste0(labm,'PV0489_T00449_NODUP','.xlsx'))
###HDL-Cholesterin, n=2641
hdlc <- read_xlsx(paste0(labm,'PV0489_T00462_NODUP','.xlsx'))
###Triglyceride, n=2641
trigly <- read_xlsx(paste0(labm,'PV0489_T00466_NODUP','.xlsx'))
###Glucose, n=1960
gluc <- read_xlsx(paste0(labm,'PV0489_T00468_NODUP','.xlsx'))
###Alkalische Phosphatase, n=2641
ap <- read_xlsx(paste0(labm,'PV0489_T00475_NODUP','.xlsx'))
###CRP, n=2634
crp <- read_xlsx(paste0(labm,'PV0489_T00495_NODUP','.xlsx'))
###IL-6, n=1843
il6 <- read_xlsx(paste0(labm,'PV0489_T00496_NODUP','.xlsx'))

#merge laboratory markers (only numeric) with che dataset ----------------------

che <- merge(che, alat[,c("ALAT_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, asat[,c("ASAT_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, ggt[,c("GGT_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, bili[,c("BILI_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, ap[,c("AP_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, gluc[,c("GLU_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, hba1c[,c("HBA1C_E_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, hdlc[,c("HDLC_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, trigly[,c("TRIG_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)

che <- merge(che, crp[,c("CRPHS_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE)
che <- merge(che, il6[,c("IL6_S_RAW_VALUE", "IL6_S_NUM_VALUE","SIC")], by="SIC", all.x=TRUE) # here we need both raw and numeric because of imputation

#merge with antropometric data -------------------------------------------------
che <- merge(che, life[,c("SIC", "adult_meda_h_atc","adult_prob_gender", "adult_prob_age", "adult_bp_sbp",
                       "adult_bp_dbp", "adult_bp_hypertension","adult_bp_known_hypertension", "bmi_bmi", 
                       "bmi_bmi_cat", "bmi_waist_hip_ratio", "tob2_smoking_status", "kardanam_f0030",
                       "kardanam_f0045", "kardanam_f0047","kardanam_f0078", "kardanam_f0109", 
                       "kardanam_f0111", "kardanam_f0112","medanam_f0131", "medanam_f0138", "medanam_f0167", 
                       "medanam_f0171", "medanam_f0179")], by="SIC", all.x=TRUE)

#organization of the new df ----------------------------------------------------
#delete columns of no interest, rename columns from anamnesis ------------------
che <- che[, -c(2,3,4)]
che <- che %>%
  rename(
    stroke_anamn = kardanam_f0030, #Hatten Sie jemals einen von einem Arzt festgestellten Schlaganfall (Gehirnschlag)?
    diabetes_anamn = kardanam_f0045,#Ist bei Ihnen jemals eine Zuckerkrankheit von einem Arzt festgestellt worden?
    diabetes_type = kardanam_f0047, #Welcher Diabetestyp liegt nach Ihrem Wissen vor?
    stroke_12m = kardanam_f0078, #Hatten Sie in den letzten 12 Monaten einen Schlaganfall (Gehirnschlag)?
    bp_anamn = kardanam_f0109, #Wurde bei Ihnen jemals von einem Arzt Bluthochdruck diagnostiziert?
    bp_12m = kardanam_f0111, #Hatten Sie in den letzten 12 Monaten Bluthochdruck?
    bp_treat = kardanam_f0112, #Werden Sie aktuell wegen eines Bluthochdrucks behandelt?
    cirrhosis = medanam_f0131, #Wurde bei Ihnen jemals von einem Arzt eine Leberzirrhose diagnostiziert?
    hepatitis = medanam_f0138, #Werden Sie aktuell wegen einer Hepatitis (A, B, C, D, E) behandelt?
    epilepsy = medanam_f0167, #Wurde bei Ihnen jemals von einem Arzt ein Krampfanfall/epileptischer Anfall diagnostiziert?
    parkinson = medanam_f0171, #Wurde bei Ihnen jemals von einem Arzt ein Parkinson-Syndrom diagnostiziert?
    MS = medanam_f0179 #Wurde bei Ihnen jemals von einem Arzt eine Multiple Sklerose diagnostiziert?
  )

################################################################################
#2. Merging of mri pseudonyms and SIC with df for future identification
################################################################################
#load mri pseudonym list
mri_pseudo <- read_xlsx(paste0("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/mri/PV489_PV-MRT-Pseudonymliste.xlsx"))
colnames(che)[1] <- "pv_pseudonym"
che <- merge(che, mri_pseudo, by="pv_pseudonym", all.x=TRUE)

#load SIC (LI-Numbers) pseudonym list-------------------------------------------
sic_pseudo <- read.csv(paste0("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/mri/pseudo_mrt_20201214.csv"))
colnames(sic_pseudo)[2] <- "mrt_pseudonym"
che <- merge(che, sic_pseudo, by="mrt_pseudonym", all.x=TRUE)
colnames(che)[44] <- "SIC"

#bring last column to the front-------------------------------------------------
che <- che%>%
  select(SIC, everything())


################################################################################
#3. Definition of hypertension and diabetes groups (for control variables) + 
# Definition of "Vascular factor" group
################################################################################

# remove hashtags around drug names
che$adult_meda_h_atc <- str_replace_all(che$adult_meda_h_atc, "#", "") 

#antihypertensive and antidiabetic medication in ATC codes----------------------
#"C02"= antihypertensiva
#"C03"= diuretics
#"C07"= beta blockers
#"C08"= ca channel blockers
#"C09"= RAAS blockers
#"A10"= antidiabetics
che <- che %>%
  mutate(BPmed = case_when(
    grepl("^C02", adult_meda_h_atc) | grepl(", C02", adult_meda_h_atc) ~ 1,
    grepl("^C03", adult_meda_h_atc) | grepl(", C03", adult_meda_h_atc) ~ 1,
    grepl("^C07", adult_meda_h_atc) | grepl(", C07", adult_meda_h_atc) ~ 1,
    grepl("^C08", adult_meda_h_atc) | grepl(", C08", adult_meda_h_atc) ~ 1,
    grepl("^C09", adult_meda_h_atc) | grepl(", C09", adult_meda_h_atc) ~ 1,
    is.na(adult_meda_h_atc) ~ NA_real_,
    TRUE ~ 0
  )) %>%
  mutate(diabmed = case_when(
    grepl("^A10", adult_meda_h_atc) | grepl(", A10", adult_meda_h_atc) ~ 1,
    is.na(adult_meda_h_atc) ~ NA_real_,
    TRUE ~ 0
  ))

#categorise subjects as hypertensive or non-hypertensive and diabetic or non-diabetic

#Hypertension: subjects will be included in the hypertension category if they fulfill any of the following criteria:
#Diagnosed hypertension in medical anamnesis
#Intake of antihypertensive medication 
#Systolic blood pressure (Bpsyst) > 160mmHg

#Diabetes mellitus: subjects will be included in the diabetes category if they fulfill any of the following criteria:
#Diagnosed diabetes mellitus in medical anamnesis 
#Intake of antidiabetic medication 
#HbA1C > = 6% 

che <- che %>%
  mutate(hypertension_cat = case_when(
    BPmed == 1 ~ 1,
    adult_bp_sbp > 160 ~ 1,
    adult_bp_known_hypertension == 1 ~ 1,
    is.na(BPmed) & is.na(adult_bp_sbp) & is.na(adult_bp_known_hypertension) ~ NA_real_,
    TRUE ~ 0
  )) %>%
  mutate(diabetes_cat = case_when(
    diabmed == 1 ~ 1, 
    HBA1C_E_NUM_VALUE >= 6 ~ 1,
    diabetes_anamn == 1 ~ 1,
    is.na(diabmed) & is.na(HBA1C_E_NUM_VALUE) & is.na(diabetes_anamn) ~ NA_real_,
    TRUE ~ 0
  ))

#check numbers
table(che$hypertension_cat) #hypertension n=853
table(che$diabetes_cat) #diabetes n=251

#make new variable that is called "vascular factor"
#subjects will be included in this if they fulfill: hypertension_cat=1 and/or diabetes_cat=1 and/or smoking status=current smoker and/or previous smoker

che <- che %>%
  mutate(vascular_factors = case_when(
    hypertension_cat == 1 ~ 1,
    diabetes_cat == 1 ~ 1,
    tob2_smoking_status == 1 | tob2_smoking_status == 2 ~ 1,
    is.na(hypertension_cat) & is.na (diabetes_cat) & is.na (tob2_smoking_status) ~ NA_real_,
    TRUE ~ 0
  ))

#check numbers
table(che$vascular_factors) #n=1168 !!!


################################################################################
#4. Definition of anticholinergic medication and che inhibitors (for exclusion)
################################################################################

#anticholinergic medication in ATC codes 
#we take only those anticholinergic medication that has systemic effects --> is taken orally 
#no mydriatics, no inhalatives (except for those defined in Coupland et al)
#"N04A" = Anticholinergica group
#"A03AA" = synthetic anticholinergics, esters with tertiary amino groups
#"A03AB" = synthetic anticholinergics, ester with quaternary ammonium compounds
#"A03CA" = synthetic anticholinergics in comb. with psycholeptics 
#"A03DA" = synthetic anticholinergics in comb. with analgetics
#"A03E" = Spasmolytics 


####anticholinergic medication acc. to Coupland et al. 2019 (defined as having strong anticholinergic properties)
#56 drugs in 11 categories

##antiarrhythmic drugs
#"C01BA03"= Disopyramide

##anthistamines
#"R06AX09"= Azatadine
#"R06AB01", "R06AB06", "R06AB51", "R06AB56" = Brompheniramine
#"R06AB04", "R06AB54" = Chlorphenamine
#"R06AA04", "R06AA54" = Clemastine
#"R06AX02", "A15AA01", "A15AA51" = Cyproheptadine
#"R06AA02", "S01GX16", "R06AA52", "N05CX07", "N05CM20", "A04AB55", "A04AB05" = Diphenhydramine 
#"N05BB01", "N05BB51", "R06AX33" = Hydroxyzine
#"R06AD01" = Alimemazine

##antidepressants
#"N06AA09", "N06AA25", "N06CA01" = Amitriptyline
#"N06AA04" = Clomipramine
#"N06AA16", "N06CA10" = Dosulepin
#"N06AA12" = Doxepin
#"N06AA02", "N06AA03" = Imipramine
#"N06AA07" = Lofepramine
#"N06AA102, "N06CA06" = Nortriptyline
#"N06AB05" = Paroxetine
#"N06AA06" = Trimipramine

##antiepileptics
#"N03AF01" = Carbamazepine
#"N03AF02" = Oxcarbazepin

##antivertigo/antiemetic
#"R06AE03", "R06AE04", "R06AE53" = Cyclizine
#"A04AB02", "A04AB52", "N07CA22" = Dimenhydrinate
#"N05AB04" = Prochlorperazine
#"A04AB58", "N05CM22", "N05CX13", "R06AD02", "R06AD52", "V03AB05" = Promethazine
#"A04AD06", "N05AA03", "N05AA04", "N05AA05" = Promazine

##antiparkinson agents
#("N04AC01") = Benzatropine
#"M03BC01", "M03BC51", ("N04AB02") = Orphenadrine
#("N04AA04)" = Procyclidine
#("N04AA01") = Trihexyphenidyl

##antipsychotics
#"N05AA01" = Chlorpromazine
#"N05AH02" = Clozapine
#"N05AA02" = Levomepromazine
#"N05AH03" = Olanzapine
#Pericyazine --> n.a.
#"N05AB03" = Perphenazine
#"N05AG02" = Pimozide
#"N05AH04" = Quetiapine
#"N05AC02" = Thioridazine
#"N05AB06" = Trifluoperazine

##bladder antimuscarinics
#"G04BD10" = Darifenacin
#"G04BD11" , "G04BD13" = Fesoterodine
#"G04BD02" = Flavoxate
#"G04BD04" = Oxybutynin
#"G04BD06" = Propiverine
#"G04BD08" , "G04CA53" = Solifenacin
#"G04BD07" = Tolterodine
#"G04BD09", "G04BD59" = Trospium

##sceletal muscle relaxants
#"M03BA03", "M03BA53", "M03BA73" = Methocarbamol
#"M03BX02" = Tizanidin

##gastrointestinal antispasmodics
#"A03AX08", "A03AX58" = Alverine
#"A03B", "A03CB" GROUP = Atropine products
#"G04BD19", "G04BD69" = Dicycloverin
#Propantheline --> already defined
#("A03BB", "A03CB" GROUP), "A03DB04", "A04AD01", "A04AD51", "N05CM05" = Scopolamine

##antimuscarinic bronchodilator drugs (inhalative!)
#"R03AL04", "R03AL07", "R03AL09", "R03AL11", "R03BB06" = Glycopyrronium 
#"C01CX10", "R01AX03", "R03AL01", "R03AL02", "R03BB01" = Impratropium

che <- che %>%
  mutate(anticholmed_cat = case_when(
    grepl("^N04A", adult_meda_h_atc) | grepl(", N04A", adult_meda_h_atc) ~ 1,
    grepl("^A03AA", adult_meda_h_atc) | grepl(", A03AA", adult_meda_h_atc) ~ 1,
    grepl("^A03AB", adult_meda_h_atc) | grepl(", A03AB", adult_meda_h_atc) ~ 1,
    grepl("^A03CA", adult_meda_h_atc) | grepl(", A03CA", adult_meda_h_atc) ~ 1,
    grepl("^A03DA", adult_meda_h_atc) | grepl(", A03DA", adult_meda_h_atc) ~ 1,
    grepl("^A03E", adult_meda_h_atc) | grepl(", A03E", adult_meda_h_atc) ~ 1,
    grepl("C01BA03", adult_meda_h_atc) ~ 1,
    grepl("R06AX09", adult_meda_h_atc) ~ 1,
    grepl("R06AB01", adult_meda_h_atc) ~ 1,
    grepl("R06AB06", adult_meda_h_atc) ~ 1,
    grepl("R06AB51", adult_meda_h_atc) ~ 1,
    grepl("R06AB56", adult_meda_h_atc) ~ 1,
    grepl("R06AB04", adult_meda_h_atc) ~ 1,
    grepl("R06AB54", adult_meda_h_atc) ~ 1,
    grepl("R06AA04", adult_meda_h_atc) ~ 1,
    grepl("R06AA54", adult_meda_h_atc) ~ 1,
    grepl("R06AX02", adult_meda_h_atc) ~ 1,
    grepl("A15AA01", adult_meda_h_atc) ~ 1,
    grepl("A15AA51", adult_meda_h_atc) ~ 1,
    grepl("R06AA02", adult_meda_h_atc) ~ 1,
    grepl("S01GX16", adult_meda_h_atc) ~ 1,
    grepl("R06AA52", adult_meda_h_atc) ~ 1,
    grepl("N05CX07", adult_meda_h_atc) ~ 1,
    grepl("N05CM20", adult_meda_h_atc) ~ 1,
    grepl("A04AB55", adult_meda_h_atc) ~ 1,
    grepl("A04AB05", adult_meda_h_atc) ~ 1,
    grepl("N05BB01", adult_meda_h_atc) ~ 1,
    grepl("N05BB51", adult_meda_h_atc) ~ 1,
    grepl("R06AX33", adult_meda_h_atc) ~ 1,
    grepl("R06AD01", adult_meda_h_atc) ~ 1,
    grepl("N06AA09", adult_meda_h_atc) ~ 1,
    grepl("N06AA25", adult_meda_h_atc) ~ 1,
    grepl("N06CA01", adult_meda_h_atc) ~ 1,
    grepl("N06AA04", adult_meda_h_atc) ~ 1,
    grepl("N06AA16", adult_meda_h_atc) ~ 1,
    grepl("N06CA10", adult_meda_h_atc) ~ 1,
    grepl("N06AA12", adult_meda_h_atc) ~ 1,
    grepl("N06AA02", adult_meda_h_atc) ~ 1,
    grepl("N06AA03", adult_meda_h_atc) ~ 1,
    grepl("N06AA07", adult_meda_h_atc) ~ 1,
    grepl("N06AA102", adult_meda_h_atc) ~ 1,
    grepl("N06CA06", adult_meda_h_atc) ~ 1,
    grepl("N06AB05", adult_meda_h_atc) ~ 1,
    grepl("N06AA06", adult_meda_h_atc) ~ 1,
    grepl("N03AF01", adult_meda_h_atc) ~ 1,
    grepl("N03AF02", adult_meda_h_atc) ~ 1,
    grepl("R06AE03", adult_meda_h_atc) ~ 1,
    grepl("R06AE04", adult_meda_h_atc) ~ 1,
    grepl("R06AE53", adult_meda_h_atc) ~ 1,
    grepl("A04AB02", adult_meda_h_atc) ~ 1,
    grepl("A04AB52", adult_meda_h_atc) ~ 1,
    grepl("N07CA22", adult_meda_h_atc) ~ 1,
    grepl("N05AB04", adult_meda_h_atc) ~ 1,
    grepl("A04AB58", adult_meda_h_atc) ~ 1,
    grepl("N05CM22", adult_meda_h_atc) ~ 1,
    grepl("N05CX13", adult_meda_h_atc) ~ 1,
    grepl("R06AD02", adult_meda_h_atc) ~ 1,
    grepl("R06AD52", adult_meda_h_atc) ~ 1,
    grepl("V03AB05", adult_meda_h_atc) ~ 1,
    grepl("A04AD06", adult_meda_h_atc) ~ 1,
    grepl("N05AA03", adult_meda_h_atc) ~ 1,
    grepl("N05AA04", adult_meda_h_atc) ~ 1,
    grepl("N05AA05", adult_meda_h_atc) ~ 1,
    grepl("M03BC01", adult_meda_h_atc) ~ 1,
    grepl("M03BC51", adult_meda_h_atc) ~ 1,
    grepl("N05AA01", adult_meda_h_atc) ~ 1,
    grepl("N05AH02", adult_meda_h_atc) ~ 1,
    grepl("N05AA02", adult_meda_h_atc) ~ 1,
    grepl("N05AH03", adult_meda_h_atc) ~ 1,
    grepl("N05AB03", adult_meda_h_atc) ~ 1,
    grepl("N05AG02", adult_meda_h_atc) ~ 1,
    grepl("N05AH04", adult_meda_h_atc) ~ 1,
    grepl("N05AC02", adult_meda_h_atc) ~ 1,
    grepl("N06AA06", adult_meda_h_atc) ~ 1,
    grepl("G04BD10", adult_meda_h_atc) ~ 1,
    grepl("G04BD11", adult_meda_h_atc) ~ 1,
    grepl("G04BD13", adult_meda_h_atc) ~ 1,
    grepl("G04BD02", adult_meda_h_atc) ~ 1,
    grepl("G04BD04", adult_meda_h_atc) ~ 1,
    grepl("G04BD06", adult_meda_h_atc) ~ 1,
    grepl("G04BD08", adult_meda_h_atc) ~ 1,
    grepl("G04CA53", adult_meda_h_atc) ~ 1,
    grepl("G04BD07", adult_meda_h_atc) ~ 1,
    grepl("G04BD09", adult_meda_h_atc) ~ 1,
    grepl("G04BD59", adult_meda_h_atc) ~ 1,
    grepl("M03BA03", adult_meda_h_atc) ~ 1,
    grepl("M03BA53", adult_meda_h_atc) ~ 1,
    grepl("M03BA73", adult_meda_h_atc) ~ 1,
    grepl("M03BX02", adult_meda_h_atc) ~ 1,
    grepl("A03AX08", adult_meda_h_atc) ~ 1,
    grepl("A03AX58", adult_meda_h_atc) ~ 1,
    grepl("^A03B", adult_meda_h_atc) | grepl(", A03B", adult_meda_h_atc) ~ 1,
    grepl("^A03CB", adult_meda_h_atc) | grepl(", A03CB", adult_meda_h_atc) ~ 1,
    grepl("G04BD19", adult_meda_h_atc) ~ 1,
    grepl("G04BD69", adult_meda_h_atc) ~ 1,
    grepl("N05CM05", adult_meda_h_atc) ~ 1,
    grepl("A03DB04", adult_meda_h_atc) ~ 1,
    grepl("A04AD01", adult_meda_h_atc) ~ 1,
    grepl("A04AD51", adult_meda_h_atc) ~ 1,
    grepl("R03AL04", adult_meda_h_atc) ~ 1,
    grepl("R03AL07", adult_meda_h_atc) ~ 1,
    grepl("R03AL09", adult_meda_h_atc) ~ 1,
    grepl("R03AL11", adult_meda_h_atc) ~ 1,
    grepl("R03BB06", adult_meda_h_atc) ~ 1,
    grepl("C01CX10", adult_meda_h_atc) ~ 1,
    grepl("R01AX03", adult_meda_h_atc) ~ 1,
    grepl("R03AL01", adult_meda_h_atc) ~ 1,
    grepl("R03AL02", adult_meda_h_atc) ~ 1,
    grepl("R03BB01", adult_meda_h_atc) ~ 1,
    is.na(adult_meda_h_atc) ~ 0,
    TRUE ~ 0
  ))

#check numbers
table(che$anticholmed_cat) #n subjects with antichol med=57

#cholinesterase inhibitors
#che inhibitors in ATC codes 
#"N06DA" = Antidementives group
#"N07AA" = Parasympathomimetics  


che <- che %>%
  mutate(cheinhibmed_cat = case_when(
    grepl("^N06DA", adult_meda_h_atc) | grepl(", N06DA", adult_meda_h_atc) ~ 1,
    grepl("^N07AA", adult_meda_h_atc) | grepl(", N07AA", adult_meda_h_atc) ~ 1,
    is.na(adult_meda_h_atc) ~ 0,
    TRUE ~ 0
  ))

#check numbers
table(che$cheinhibmed_cat) #n subjects with cheinhib med=0

################################################################################
#5. Definition of disrupted liver function (for exclusion)
#The following conditions in three or more of the following tests will lead to an exclusion (from Zivkovic et al 2018): 
#ASAT >100U/L (>1.67 µkat/l)
#ALAT > 100U/L (>1.67 µkat/l)
#GGT > 100U/L (>1.67 µkat/l)
#AP > 200U/L (>3.34 µkat/l) 
    # SI units to U/L --> U/l x 0,0167 = μkat/l
#total bilirubin > 2mg/dl (>34.2 µmol/l)
    # mg/dl to µmol/l --> mg/dl x 17,1 = μmol/l

#Furthermore, patients with acute liver cirrhosis (MEDANAM_F0131) or acute hepatitis (MEDANAM_F0138) will be excluded
################################################################################

#Define the exclusion conditions
che <- che %>%
  mutate(asat_elev =case_when(
    ASAT_S_NUM_VALUE >= 1.67 ~ 1,
    is.na(ASAT_S_NUM_VALUE) ~ NA_real_,
    TRUE ~ 0
  )) %>%
  mutate(alat_elev =case_when(
    ALAT_S_NUM_VALUE >= 1.67 ~ 1,
    is.na(ALAT_S_NUM_VALUE) ~ NA_real_,
    TRUE ~ 0
  )) %>%
  mutate(ggt_elev =case_when(
    GGT_S_NUM_VALUE >= 1.67 ~ 1,
    is.na(GGT_S_NUM_VALUE) ~ NA_real_,
    TRUE ~ 0
  )) %>%
  mutate(ap_elev =case_when(
    AP_S_NUM_VALUE >= 3.34 ~ 1,
    is.na(AP_S_NUM_VALUE) ~ NA_real_,
    TRUE ~ 0
  )) %>%
  mutate(bili_elev =case_when(
    BILI_S_NUM_VALUE >= 34.2 ~ 1,
    is.na(BILI_S_NUM_VALUE) ~ NA_real_,
    TRUE ~ 0
  )) 

# Calculate the number of conditions met for each subject
che <- che %>%
  mutate(conditions_met = rowSums(select(., ends_with("_elev")), na.rm = TRUE))

# Define the exclusion threshold (3 or more conditions met)
exclusion_threshold <- 3

# Create an "Exclusion" column based on the threshold
# 1 = exclude, 0 = don't exclude
che <- che %>%
  mutate(excl_elev_liver = ifelse(conditions_met >= exclusion_threshold, "1", "0"))

#check numbers
table(che$excl_elev_liver) #n=0

# define disrupted liver function
che <- che%>%
  mutate(disr_liver_function = case_when(
    excl_elev_liver == "1" ~ 1,
    cirrhosis == 1 ~ 1,
    hepatitis == 1 ~ 1,
    is.na(excl_elev_liver) & is.na(cirrhosis) & is.na(hepatitis) ~ NA_real_,
    TRUE ~ 0
  ))

#check numbers
table(che$disr_liver_function) #n=15

#write new compiled df-------------------------------------------------------------------
setwd("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/")
write.csv(che, file ="life_lab_compiled.csv", row.names = FALSE)


################################################################################
#6. Apply exclusion due to medication or liver function
# This exclusion applies for all research questions
################################################################################

che <- subset(che, che$anticholmed_cat == 0 & che$cheinhibmed_cat == 0) # 1532 --> 1475
che <- subset(che, che$disr_liver_function == 0) # 1475 --> 1460


################################################################################
#7. Preparation of data for sample 1
# Exclusion due to missing data in CRP and BMI 
# Check for normal distribution, log-transform 
################################################################################

#check NA for pChE
sum(is.na(che$total_activity)) # n=0

#check NA for CRP 
sum(is.na(che$CRPHS_S_NUM_VALUE)) #n=5
che <- subset(che, !is.na(CRPHS_S_NUM_VALUE))  # 1460 --> 1455

#check NA for BMI
sum(is.na(che$bmi_bmi)) #n=4
che <- subset(che, !is.na(bmi_bmi)) # 1455 --> 1451

#check CRP ---------------------------------------------------------------------

table(che$CRPHS_S_NUM_VALUE) #highest 146,92 mg/l, reference= <5mg/l
table(che$CRPHS_S_NUM_VALUE<5.0) # 163 are above the reference

#Distribution
qqnorm(che$CRPHS_S_NUM_VALUE)

#Skewness
mean(che$CRPHS_S_NUM_VALUE, na.rm=TRUE) #2.79
median(che$CRPHS_S_NUM_VALUE, na.rm=TRUE) #1.49
#conclusion: mean>median --> right-skewed (as expected)

#log-transform (natural log, ln)
che$crp_ln <- log(che$CRPHS_S_NUM_VALUE)

#plot again
qqnorm(che$crp_ln)

#check BMI ---------------------------------------------------------------------

#Distribution
qqnorm(che$bmi_bmi)

#check pChE --------------------------------------------------------------------

#Distribution
qqnorm(che$total_activity)
qqnorm(che$AChE_activity)


#write df for sample 1----------------------------------------------------------
setwd("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/")
write.csv(che, file ="sample_1.csv", row.names = FALSE)


################################################################################
#8.Preparation of data for sample 2 (=sub-sample)
# Exclusion due to missing data in IL-6
# Check for normal distribution, log-transform
# Imputation of IL-6 below threshold
################################################################################

#che <- read.csv(paste0("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/sample_1.csv"))

#check NA for IL-6
sum(is.na(che$IL6_S_NUM_VALUE)) # n=409
che <- subset(che, !is.na(IL6_S_NUM_VALUE)) # 1451 --> 1042

table(che$IL6_S_NUM_VALUE) #highest 110 pg/ml, reference= <7pg/ml
table(che$IL6_S_NUM_VALUE>=7.0) # 94 are above the reference
table(che$IL6_S_RAW_VALUE<=1.50) #368 are below the threshold --> impute them

# Create a new column to indicate whether a value is equal/below 1.50 or not
che$il6_subthreshold <- ifelse(che$IL6_S_NUM_VALUE <= 1.50, 1, 0)

#Distribution
qqnorm(che$IL6_S_NUM_VALUE)

#Skewness
mean(che$IL6_S_NUM_VALUE) #3.90
median(che$IL6_S_NUM_VALUE) #2.77
#conclusion: mean>median --> right-skewed (as expected)

#log-transform (natural log, ln)
che$il6_ln <- log(che$IL6_S_NUM_VALUE)
qqnorm(che$il6_ln)

#plot a density plot to see better
ggplot(che, aes(x = il6_ln)) +
  geom_density(alpha = 0.5)


#Imputation of below cutoff (BC)= <1,50pg/ml values -----------------------------
#This novel approach uses Mlog algorithm as described by Herbers et al. 2021
#It uses censored regression fitting to the observed data and imputes values from the respective censored intervals
#required packages: fitdistrplus for fitting censored lognormal distributions and EnvStats to perform random draws from an interval of a lognormal distribution

#install.packages("remotes") 
#remotes::install_github("nx10/lnormimp-r")
library(lnormimp)

#plot
plot(density(che$IL6_S_NUM_VALUE), col = "blue", lty = "dashed", main = "Probability-density")

#define lower cutoff
lc <- 1.50
uc <- 7.0 

#mark them in the diagram
abline(v = lc, col = "darkgray")
abline(v = uc, col = "darkgray")


below_lower <- che$IL6_S_NUM_VALUE<=lc
n_below <- sum(below_lower) #n=368

above_upper <- che$IL6_S_NUM_VALUE>uc 
n_above <- sum(above_upper) #n=93 

data_censored <- che$IL6_S_NUM_VALUE[!(below_lower| above_upper)] 
data.frame(n = length(che$IL6_S_NUM_VALUE),
           n_below = n_below,
           n_above = n_above,
           n_censored = length(data_censored)
           )
#n_below=368; n_above=93; n_censored=581

#draw the censored distribution to the diagram
lines(density(data_censored), col = "red", lty = "dotted")

#reconstruct original data from the censored by imputation
che$il6_subthresh_impu=lnormimp(
  data_censored,
  censn = c(n_below, n_above),
  cutoff = c(lc, uc)
)

#plot the new imputed data
lines(density(che$il6_subthresh_impu), col = "darkgreen")

# Create a new column based on conditions
che$il6_imputed <- ifelse(che$IL6_S_NUM_VALUE > 1.50, che$IL6_S_NUM_VALUE, che$il6_subthresh_impu)

#plot again --> not normally distributed
qqnorm(che$il6_imputed)

#log-transform (natural log, ln)
che$il6_imp_ln <- log(che$il6_imputed)

#plot again
qqnorm(che$il6_imp_ln)



################################################################################
#9.Composite score for inflammation
# unclear...open for discussion
################################################################################

#check for correlation between CRP and IL-6
cor(che$il6_imp_ln, che$crp_ln, method = 'pearson') # r=0.27
plot(che$il6_imp_ln, che$crp_ln)

#infl_score=lm(il6_ln ~ crp_ln, data = che)
#summary(infl_score)


#write df for sample 2----------------------------------------------------------
setwd("/data/hu_eisenberg/Desktop/ache_inflammation_brain/pv_0489_data/")
write.csv(che, file ="sample_2.csv", row.names = FALSE)










